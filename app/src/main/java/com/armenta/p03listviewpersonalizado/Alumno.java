package com.armenta.p03listviewpersonalizado;

import java.io.Serializable;
import java.util.ArrayList;

public class Alumno implements Serializable{
    private int id;
    private String carrera;
    private String nombre;
    private Integer img;
    private String matricula;
    private String imagenUrl;

    public Alumno(){
        this.carrera = "";
        this.nombre = "";
        this.img = 0;
        this.matricula = "";
        this.imagenUrl = "";
    }

    public Alumno(String carrera, String nombre, Integer img, String matricula, String imagenUrl){
        this.carrera = carrera;
        this.nombre = nombre;
        this.img = img;
        this.matricula = matricula;
        this.imagenUrl = imagenUrl;
    }
    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getCarrera() { return carrera; }
    public void setCarrera(String carrera) { this.carrera = carrera; }
    public String getNombre() { return nombre; }
    public void setNombre(String nombre) { this.nombre = nombre; }
    public int getImg() { return img; }
    public void setImg(Integer img) { this.img = img; }
    public String getMatricula() { return matricula; }
    public void setMatricula(String matricula) { this.matricula = matricula; }
    public String getImagenUrl() {
        return imagenUrl;
    }
    public void setImagenUrl(String imagenUrl) {
        this.imagenUrl = imagenUrl;
    }

    public static ArrayList<Alumno> llenarAlumnos (){
        ArrayList<Alumno> alumnos = new ArrayList<>();
        String carrera = "Ing. Tec. Información";
        return alumnos;
    }
}
